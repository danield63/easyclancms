<?php

namespace AppBundle\Entity;
use AppBundle\Entity\SteamMember;

/**
 * SteamGroup
 *
 */
class SteamGroup
{

  private $groupname;
  private $memberlist;
  function __construct($groupname)
  {
      $xml = simplexml_load_file("http://steamcommunity.com/groups/".$groupname."/memberslistxml/?xml=1");
      if ($groupname!=null) {
        $this->groupname = $groupname;
        $steamids = $xml->members->steamID64;
        foreach ($steamids as $item) {
          $member = new SteamMember($item);
          if (!empty($member->getSteamID())) {
            $this->memberlist[] = $member;
          }
        }
      }
  }
  /**
   * Get groupname
   *
   * @return groupname
   */
  public function getGroupname()
  {
      return $this->groupname;
  }

  /**
   * Get memberlist
   *
   * @return memberlist
   */
  public function getMemberlist()
  {
      return $this->memberlist;
  }
}
