<?php

namespace AppBundle\Entity;

/**
 * SteamGroup
 *
 */
class SteamMember
{

  private $steamID;
  private $onlineState;
  private $avatarMedium;
  private $avatarFull;

  function __construct($steamid)
  {
    if ($steamid!=null) {
      $url = "http://steamcommunity.com/profiles/".$steamid."/?xml=1";
      $xml = simplexml_load_file($url);
      $this->steamID = (string)$xml->steamID;
      $this->onlineState = (string)$xml->onlineState;
      $this->avatarMedium = (string)$xml->avatarMedium;
      $this->avatarFull = (string)$xml->avatarFull;
      }
    }

    public function getSteamID(){
      return $this->steamID;
    }

    public function setSteamID($steamID){
      $this->steamID = $steamID;
    }

    public function getOnlineState(){
      return $this->onlineState;
    }

    public function setOnlineState($onlineState){
      $this->onlineState = $onlineState;
    }

    public function getAvatarMedium(){
      return $this->avatarMedium;
    }

    public function setAvatarMedium($avatarMedium){
      $this->avatarMedium = $avatarMedium;
    }

    public function getAvatarFull(){
      return $this->avatarFull;
    }

    public function setAvatarFull($avatarFull){
      $this->avatarFull = $avatarFull;
    }
}
