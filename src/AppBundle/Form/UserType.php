<?php
// src/AppBundle/Form/UserType.php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserType extends AbstractType
{
  public function __construct($options = null) {
      $this->options = $options;
  }
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $passwordOptions = array(
        'type' => PasswordType::class,
        'first_options'  => array('label' => 'Password'),
        'second_options' => array('label' => 'Repeat Password'),
        'required' => true,
      );
      $userId = $options['data']->getId();
      if (!empty($userId)) {
        $passwordOptions['required'] = false;
      }

      $builder
          ->add('email', EmailType::class)
          ->add('username', TextType::class)
          ->add('role', ChoiceType::class, array(
                'choices'  => array(
                    'USER' => 'ROLE_USER',
                    'MEMBER' => 'ROLE_MEMBER',
                    'ADMIN' => 'ROLE_ADMIN',)
                  ))
          ->add('plainPassword', RepeatedType::class, $passwordOptions);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
        ));
    }
}
