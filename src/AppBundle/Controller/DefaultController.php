<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\SteamGroup;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // get data
        $user = $this->getUser();
        $Steamgroup = new SteamGroup('air_commando');
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository('AppBundle:Post')->findAll();

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'steamgroup' => $Steamgroup,
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/steam", name="steam")
     */
    public function steamAction()
    {
        $Steamgroup = new SteamGroup('air_commando');

        return $this->render('steam/list.html.twig', array(
            'steamgroup' => $Steamgroup,
        ));
    }

    /**
     * @Route("/admin")
     */
    public function adminAction()
    {
        // replace this example code with whatever you need
        return new Response('<html><body>Admin page !</html></body>');
    }
}
