<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
* Security controller.
*/
class SecurityController extends Controller
{
  /**
  * @Route("/login", name="login")
  */
  public function loginAction(Request $request)
  {
    $authenticationUtils = $this->get('security.authentication_Utils');
// TODO: Améliorer le message d'erreur
    // récupération de l'éventuelle erreur de login_path
    $error = $authenticationUtils->getLastAuthenticationError();
    // récupération du dernier mail rentré apr l'utilisateur
    $lastUsername = $authenticationUtils->getLastUsername();
    return $this->render(
            'security/login.html.twig',
            array('last_username' => $lastUsername ,
                  'error' => $error, )
            );

  }

}
