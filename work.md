# Process
## Environnement
- ajout des librairies
- ajout des assets
## Paramètrage
- database
- mailer
- doctrine
## Model
### Entities
- décrire les entités
- décrire les relations
- décrire les contraintes pour les validateurs
- générer la base de données
- générer les CRUD
## Controller
- adapter les contrôleurs
## Views
- adapter la vue de base
- adapter les vues CRUD générées
- adapter le CSS
## Sécurité
- providers
- encoders
- firewalls
- access control
- login
- roles hierarchie
## Internationalisation
- config.yml translator
## test automatiques
- intégrer le transaltor
par appel au container du client
