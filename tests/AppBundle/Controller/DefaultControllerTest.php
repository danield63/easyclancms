<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\TRanslation\Translator;
use PHPUnit\Framework\TestCase;

class DefaultControllerTest extends WebTestCase
{
    protected static $trs;

    public static function setUpBeforeClass()
    {
        $client = static::createClient();
        self::$trs = $client->getContainer()->get('translator');
    }

    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $container = static::$kernel->getContainer();

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $translated = self::$trs->trans('home');
        $this->assertContains($translated, $crawler->filter('#topmenu a')->text());
        $this->checkHtmlContains('login',$crawler);
        $this->checkHtmlContains('register',$crawler);

    }

    /**
     *
     * @depends testIndex
     */
    public function testLogin()
    {
      // $this->markTestSkipped( 'PHPUnit will skip this test method' );
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
        $link = $crawler->selectLink(self::$trs->trans('login'))->link();
        $crawler = $client->click($link);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->checkHtmlContains('Username',$crawler);

    }
    /**
     *
     * @depends testIndex
     */
    public function testRegister()
    {
      // $this->markTestSkipped( 'PHPUnit will skip this test method' );
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
        $link = $crawler->selectLink(self::$trs->trans('register'))->link();
        $crawler = $client->click($link);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->checkHtmlContains('Username',$crawler);
        $this->checkHtmlContains('Password',$crawler);
        $this->checkHtmlContains('Repeat Password',$crawler);
        $this->checkHtmlContains('Email',$crawler,'email');

    }

    public function checkHtmlContains($string, $crawler, $msg = null)
    {
      // var_dump(self::$trs->trans('Email'));
      $this->assertGreaterThan(0, $crawler
           ->filter('html:contains('.self::$trs->trans($string).')')
           ->count(),$msg);
    }
}
