<?php

 namespace Tests\AppBundle\Entity;

 use AppBundle\Entity\SteamGroup;

 /**
  *
  */
 class SteamGroupTest extends \PHPUnit_Framework_TestCase
 {

   public function testgetGroupnameNotEmpty()
   {
     $instance = new SteamGroup("air_commando");
     $result = $instance->getGroupname();

     $this->assertNotEmpty($result);
   }
   public function testgetGroupname()
   {
     $instance = new SteamGroup("air_commando");
     $actual = $instance->getGroupname();
     $expected = "air_commando";
     $this->assertEquals($expected, $actual);
   }
   public function testgetMemberlist()
   {
     $instance = new SteamGroup("air_commando");
     $actual = $instance->getMemberlist();
     $this->assertGreaterThan(0, count($actual));
   }
 }
